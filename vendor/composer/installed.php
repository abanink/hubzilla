<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '188975ccbd3a9c584238c35dd8c5a8fda7718199',
    'name' => 'zotlabs/hubzilla',
  ),
  'versions' => 
  array (
    'blueimp/jquery-file-upload' => 
    array (
      'pretty_version' => 'v10.31.0',
      'version' => '10.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0740f81829698b84efe17e72501e0f420ea0d611',
    ),
    'bshaffer/oauth2-server-php' => 
    array (
      'pretty_version' => 'v1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a0c8000d4763b276919e2106f54eddda6bc50fa',
    ),
    'commerceguys/intl' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '47d5d6d60d0cc25f867e337ce229a228bf6be6f8',
    ),
    'desandro/imagesloaded' => 
    array (
      'pretty_version' => 'v4.1.4',
      'version' => '4.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '67c4e57453120935180c45c6820e7d3fbd2ea1f9',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'league/html-to-markdown' => 
    array (
      'pretty_version' => '4.10.0',
      'version' => '4.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0868ae7a552e809e5cd8f93ba022071640408e88',
    ),
    'lukasreschke/id3parser' => 
    array (
      'pretty_version' => 'v0.0.3',
      'version' => '0.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '62f4de76d4eaa9ea13c66dacc1f22977dace6638',
    ),
    'michelf/php-markdown' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'pear/text_languagedetect' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb9ff6f4970f686fac59081e916b456021fe7ba6',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '3.9.3',
      'version' => '3.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e1633a6964b48589b142d60542f9ed31bd37a92',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '3.9.3',
      ),
    ),
    'sabre/dav' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5736f943c90d8d73d04cd8944d8c913811dc7360',
    ),
    'sabre/event' => 
    array (
      'pretty_version' => '5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd00a17507af0e7544cfe17096372f5d733e3b276',
    ),
    'sabre/http' => 
    array (
      'pretty_version' => '5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '23446999f1f6e62892bbd89745070aa902dd3539',
    ),
    'sabre/uri' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '059d11012603be2e32ddb7543602965563ddbb09',
    ),
    'sabre/vobject' => 
    array (
      'pretty_version' => '4.3.1',
      'version' => '4.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a7feca8311462e5da16952454e420b92c20d3586',
    ),
    'sabre/xml' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41c6ba148966b10cafd31d1a4e5feb1e2138d95c',
    ),
    'simplepie/simplepie' => 
    array (
      'pretty_version' => '1.5.5',
      'version' => '1.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae49e2201b6da9c808e5dac437aca356a11831b4',
    ),
    'smarty/smarty' => 
    array (
      'pretty_version' => 'v3.1.36',
      'version' => '3.1.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd148f7ade295014fff77f89ee3d5b20d9d55451',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'twbs/bootstrap' => 
    array (
      'pretty_version' => 'v4.5.2',
      'version' => '4.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f2480a90ab911babc53039835fe78c6fc12646d',
    ),
    'twitter/bootstrap' => 
    array (
      'replaced' => 
      array (
        0 => 'v4.5.2',
      ),
    ),
    'zotlabs/hubzilla' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '188975ccbd3a9c584238c35dd8c5a8fda7718199',
    ),
  ),
);
